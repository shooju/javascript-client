# Shooju javascript client

## Install
To install shooju javascript client use following command
```
npm i shooju-client --save
```


## Usage
```
var shoojuClient = require("shooju-client");

var sjclient = new shoojuClient(server, user, api_key);
```

## Running Tests
```
npm run tests
```