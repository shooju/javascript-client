describe("ShoojuClient.helpers", function () {

  it("'parseQueryAndOperators' parses queries and operators", function () {
    var q = 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe@localize';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe',
      op: '@localize'
    }));

    q = 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe',
      op: null
    }));

    q = 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe@localize@A:M@filter:1-6M';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: 'chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe',
      op: '@localize@A:M@filter:1-6M'
    }));

    q = '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}',
      op: null
    }));

    q = '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}@localize@A:M@filter:1-6M';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}',
      op: '@localize@A:M@filter:1-6M'
    }));

    q = '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}@localize@A:M@filter:1-6M';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: '={{chart_name="GTFP_Retail Prices" components="Ex-Refinery Price" "fuel_type@sort:term.asc"=E85 region=Europe}}',
      op: '@localize@A:M@filter:1-6M'
    }));

    q = "=sjdf('xxxx@A:1y') * 100";
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: "=sjdf('xxxx@A:1y') * 100",
      op: null
    }));

    q = '';
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: '',
      op: null
    }));

    q = null;
    expect(ShoojuClient.helpers.parseQueryAndOperators(q)).toEqual(jasmine.objectContaining({
      q: null,
      op: null
    }));
  });

  it("'escapeCharsExp' is defined", function () {
    expect(ShoojuClient.constants.escapeCharsExp).toBeDefined();
  });

  it("'escapeCharsExp' matches lines", function () {
    // /[~><\*(),:=\@\s]/
    expect('abc~def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc>def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc<def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc*def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc(def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc)def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc,def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc:def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc=def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc@def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\ndef').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\tdef').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\rdef').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc;def').toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc&def').toMatch(ShoojuClient.constants.escapeCharsExp);

    expect('abc[def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc]def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\'def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\"def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc_def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc.def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc\\def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc/def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc#def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc%def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc$def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc|def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc?def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc`def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abc!def').not.toMatch(ShoojuClient.constants.escapeCharsExp);
    expect('abcüdef').not.toMatch(ShoojuClient.constants.escapeCharsExp);
  });

  it("merge is defined", function () {
    expect(ShoojuClient.helpers.merge).toBeDefined();
  });

  // #0 in C#
  it("'merge' test 0", function () {
    var data = [
      {
        points: [
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
        ]
      },
      {
        points: [
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6],
        ]
      }
    ];
    var expected = [
      [Date.UTC(2014, 11, 2, 1, 0, 0), 1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), 2, 2],
      [Date.UTC(2014, 11, 2, 3, 0, 0), 3, 3],
      [Date.UTC(2014, 11, 2, 4, 0, 0), 4, 4],
      [Date.UTC(2014, 11, 2, 5, 0, 0), 5, 5],
      [Date.UTC(2014, 11, 2, 6, 0, 0), undefined, 6]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #1 in C#
  it("'merge' test 1", function () {
    var data = [
      {
        points: [
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5]
        ]
      },
      {
        points: [
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6]
        ]
      },
      {
        points: [
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.1],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.1],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.2],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.2],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4]
        ]
      },
      {
        points: [
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6]
        ]
      },
      {
        points: [
          [Date.UTC(2013, 11, 02, 2, 0, 0), 2013],
          [Date.UTC(2018, 11, 02, 6, 0, 0), 2018]
        ]
      },
      {
        points: [
          [Date.UTC(2013, 11, 02, 2, 0, 0), 2013]
        ]
      },
      {
        points: [
          [Date.UTC(2018, 11, 02, 2, 0, 0), 2018]
        ]
      }
    ];
    var expected = [
      [Date.UTC(2013, 11, 2, 2, 0, 0), undefined, undefined, undefined, undefined, 2013, 2013, undefined],
      [Date.UTC(2014, 11, 2, 1, 0, 0), 1, undefined, 1, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), 2, 2, 2.1, 2.1, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.1, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.1, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.1, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), undefined, undefined, 2.2, 2.2, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.2, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.2, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.2, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 3, 0, 0), 3, 3, 3, undefined, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 4, 0, 0), 4, 4, 4, 4, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 5, 0, 0), 5, 5, undefined, 5, undefined, undefined, undefined],
      [Date.UTC(2014, 11, 2, 6, 0, 0), undefined, 6, undefined, 6, undefined, undefined, undefined],
      [Date.UTC(2018, 11, 2, 2, 0, 0), undefined, undefined, undefined, undefined, undefined, undefined, 2018],
      [Date.UTC(2018, 11, 2, 6, 0, 0), undefined, undefined, undefined, undefined, 2018, undefined, undefined]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #2 in C#
  it("'merge' test 2", function () {
    var data = [
      {
        //non-localized series
        points: [
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5]
        ]
      },
      {
        points: [
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6]
        ]
      }, {
        points: [
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.1],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.1],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.2],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.2],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4]
        ]
      }, {
        points: [
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6]
        ]
      }
    ];
    var expected = [
      [Date.UTC(2014, 11, 2, 1, 0, 0), 1, undefined, 1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), 2, 2, 2.1, 2.1],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), undefined, undefined, 2.2, 2.2],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.2, undefined],
      [Date.UTC(2014, 11, 2, 3, 0, 0), 3, 3, 3, 3],
      [Date.UTC(2014, 11, 2, 4, 0, 0), 4, 4, 4, 4],
      [Date.UTC(2014, 11, 2, 5, 0, 0), 5, 5, undefined, 5],
      [Date.UTC(2014, 11, 2, 6, 0, 0), undefined, 6, undefined, 6]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #3 in C#
  it("'merge' test 3", function () {
    var data = [
      {
        points:
          [
            [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2015, 11, 02, 1, 0, 0), 1],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
          ]
      },
      {
        points:
          [
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2014, 11, 02, 6, 0, 0), 6],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2015, 11, 02, 6, 0, 0), 6],
          ]
      },
      {
        points:
          [
            [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
            [Date.UTC(2014, 11, 02, 2, 15, 0), 215.1],
            [Date.UTC(2014, 11, 02, 2, 30, 0), 230.1],
            [Date.UTC(2014, 11, 02, 2, 45, 0), 245.1],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2014, 11, 02, 2, 15, 0), 215.2],
            [Date.UTC(2014, 11, 02, 2, 30, 0), 230.2],
            [Date.UTC(2014, 11, 02, 2, 45, 0), 245.2],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2015, 11, 02, 1, 0, 0), 1],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.1],
            [Date.UTC(2015, 11, 02, 2, 15, 0), 215.1],
            [Date.UTC(2015, 11, 02, 2, 30, 0), 230.1],
            [Date.UTC(2015, 11, 02, 2, 45, 0), 245.1],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2015, 11, 02, 2, 15, 0), 215.2],
            [Date.UTC(2015, 11, 02, 2, 30, 0), 230.2],
            [Date.UTC(2015, 11, 02, 2, 45, 0), 245.2],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2015, 11, 02, 4, 0, 0), 4]
          ]
      },
      {
        points:
          [
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2014, 11, 02, 6, 0, 0), 6],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.1],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
            [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2015, 11, 02, 6, 0, 0), 6]
          ]
      }
    ];

    var expected = [
      [Date.UTC(2014, 11, 2, 1, 0, 0), 1, undefined, 1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), 2, 2, 2.1, 2.1],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0), undefined, undefined, 2.2, 2.2],
      [Date.UTC(2014, 11, 2, 2, 15, 0), undefined, undefined, 215.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0), undefined, undefined, 230.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 45, 0), undefined, undefined, 245.2, undefined],
      [Date.UTC(2014, 11, 2, 3, 0, 0), 3, 3, 3, 3],
      [Date.UTC(2014, 11, 2, 4, 0, 0), 4, 4, 4, 4],
      [Date.UTC(2014, 11, 2, 5, 0, 0), 5, 5, undefined, 5],
      [Date.UTC(2014, 11, 2, 6, 0, 0), undefined, 6, undefined, 6],
      [Date.UTC(2015, 11, 2, 1, 0, 0), 1, undefined, 1, undefined],
      [Date.UTC(2015, 11, 2, 2, 0, 0), 2, 2, 2.1, 2.1],
      [Date.UTC(2015, 11, 2, 2, 15, 0), undefined, undefined, 215.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 30, 0), undefined, undefined, 230.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 45, 0), undefined, undefined, 245.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 0, 0), undefined, undefined, 2.2, 2.2],
      [Date.UTC(2015, 11, 2, 2, 15, 0), undefined, undefined, 215.2, undefined],
      [Date.UTC(2015, 11, 2, 2, 30, 0), undefined, undefined, 230.2, undefined],
      [Date.UTC(2015, 11, 2, 2, 45, 0), undefined, undefined, 245.2, undefined],
      [Date.UTC(2015, 11, 2, 3, 0, 0), 3, 3, 3, 3],
      [Date.UTC(2015, 11, 2, 4, 0, 0), 4, 4, 4, 4],
      [Date.UTC(2015, 11, 2, 5, 0, 0), 5, 5, undefined, 5],
      [Date.UTC(2015, 11, 2, 6, 0, 0), undefined, 6, undefined, 6]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #4 in C#
  it("'merge' test 4", function () {
    var data = [
      {
        points: [
          [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2015, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2015, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1],
        ]
      },
      {
        points: [
          [Date.UTC(2015, 11, 02, 6, 0, 0), 6],
          [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2015, 11, 02, 2, 0, 0), 2],
          [Date.UTC(2014, 11, 02, 6, 0, 0), 6],
          [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2],
        ]
      },
      {
        points: [
          [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2015, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2015, 11, 02, 2, 45, 0), 245.2],
          [Date.UTC(2015, 11, 02, 2, 30, 0), 230.2],
          [Date.UTC(2015, 11, 02, 2, 15, 0), 215.2],
          [Date.UTC(2015, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2015, 11, 02, 2, 45, 0), 245.1],
          [Date.UTC(2015, 11, 02, 2, 30, 0), 230.1],
          [Date.UTC(2015, 11, 02, 2, 15, 0), 215.1],
          [Date.UTC(2015, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2015, 11, 02, 1, 0, 0), 1],
          [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
          [Date.UTC(2014, 11, 02, 3, 0, 0), 3],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.2],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.2],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.2],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
          [Date.UTC(2014, 11, 02, 2, 45, 0), 245.1],
          [Date.UTC(2014, 11, 02, 2, 30, 0), 230.1],
          [Date.UTC(2014, 11, 02, 2, 15, 0), 215.1],
          [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1],
          [Date.UTC(2014, 11, 02, 1, 0, 0), 1]
        ]
      },
      {
        points:
          [
            [Date.UTC(2015, 11, 02, 6, 0, 0), 6],
            [Date.UTC(2015, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2015, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3.2],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2015, 11, 02, 3, 0, 0), 3.1],
            [Date.UTC(2015, 11, 02, 2, 0, 0), 2.1],
            [Date.UTC(2014, 11, 02, 6, 0, 0), 6],
            [Date.UTC(2014, 11, 02, 5, 0, 0), 5],
            [Date.UTC(2014, 11, 02, 4, 0, 0), 4],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3.2],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.2],
            [Date.UTC(2014, 11, 02, 3, 0, 0), 3.1],
            [Date.UTC(2014, 11, 02, 2, 0, 0), 2.1]
          ]
      }
    ];
    var expected = [
      [Date.UTC(2015, 11, 2, 6, 0, 0, 0), undefined, 6, undefined, 6],
      [Date.UTC(2015, 11, 2, 5, 0, 0, 0), 5, 5, undefined, 5],
      [Date.UTC(2015, 11, 2, 4, 0, 0, 0), 4, 4, 4, 4],
      [Date.UTC(2015, 11, 2, 3, 0, 0, 0), 3, 3, 3, 3.2],
      [Date.UTC(2015, 11, 2, 2, 45, 0, 0), undefined, undefined, 245.2, undefined],
      [Date.UTC(2015, 11, 2, 2, 30, 0, 0), undefined, undefined, 230.2, undefined],
      [Date.UTC(2015, 11, 2, 2, 15, 0, 0), undefined, undefined, 215.2, undefined],
      [Date.UTC(2015, 11, 2, 2, 0, 0, 0), 2, 2, 2.2, 2.2],
      [Date.UTC(2015, 11, 2, 3, 0, 0, 0), undefined, undefined, undefined, 3.1],
      [Date.UTC(2015, 11, 2, 2, 45, 0, 0), undefined, undefined, 245.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 30, 0, 0), undefined, undefined, 230.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 15, 0, 0), undefined, undefined, 215.1, undefined],
      [Date.UTC(2015, 11, 2, 2, 0, 0, 0), undefined, undefined, 2.1, 2.1],
      [Date.UTC(2015, 11, 2, 1, 0, 0, 0), 1, undefined, 1, undefined],
      [Date.UTC(2014, 11, 2, 6, 0, 0, 0), undefined, 6, undefined, 6],
      [Date.UTC(2014, 11, 2, 5, 0, 0, 0), 5, 5, undefined, 5],
      [Date.UTC(2014, 11, 2, 4, 0, 0, 0), 4, 4, 4, 4],
      [Date.UTC(2014, 11, 2, 3, 0, 0, 0), 3, 3, 3, 3.2],
      [Date.UTC(2014, 11, 2, 2, 45, 0, 0), undefined, undefined, 245.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0, 0), undefined, undefined, 230.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 15, 0, 0), undefined, undefined, 215.2, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0, 0), 2, 2, 2.2, 2.2],
      [Date.UTC(2014, 11, 2, 3, 0, 0, 0), undefined, undefined, undefined, 3.1],
      [Date.UTC(2014, 11, 2, 2, 45, 0, 0), undefined, undefined, 245.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 30, 0, 0), undefined, undefined, 230.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 15, 0, 0), undefined, undefined, 215.1, undefined],
      [Date.UTC(2014, 11, 2, 2, 0, 0, 0), undefined, undefined, 2.1, 2.1],
      [Date.UTC(2014, 11, 2, 1, 0, 0, 0), 1, undefined, 1, undefined]
    ];
    var reverse = true;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #5 in C#
  it("'merge' test 5", function () {
    var data = [
      {
        points: [
          [Date.UTC(2017, 10, 29, 0, 0, 0), 1],
          [Date.UTC(2017, 10, 29, 0, 30, 0), 2],
          [Date.UTC(2017, 10, 29, 1, 0, 0), 3.1],
          [Date.UTC(2017, 10, 29, 1, 30, 0), 4.1],
          [Date.UTC(2017, 10, 29, 1, 0, 0), 3.2],
          [Date.UTC(2017, 10, 29, 1, 30, 0), 4.2],
          [Date.UTC(2017, 10, 29, 2, 0, 0), 5],
          [Date.UTC(2017, 10, 29, 2, 30, 0), 6],
        ]
      },
      {
        points:
          [
            [Date.UTC(2017, 10, 29, 0, 0, 0), 1],
            [Date.UTC(2017, 10, 29, 1, 0, 0), 3],
            [Date.UTC(2017, 10, 29, 2, 0, 0), 5],
            [Date.UTC(2017, 10, 29, 3, 0, 0), 7],
            [Date.UTC(2017, 10, 29, 4, 0, 0), 8],
            [Date.UTC(2017, 11, 05, 01, 0, 0), 9],
            [Date.UTC(2017, 11, 05, 01, 0, 0), 10]
          ]
      },
    ];
    var expected = [
      [Date.UTC(2017, 10, 29, 0, 0, 0), 1, 1],
      [Date.UTC(2017, 10, 29, 0, 30, 0), 2, undefined],
      [Date.UTC(2017, 10, 29, 1, 0, 0), 3.1, 3],
      [Date.UTC(2017, 10, 29, 1, 30, 0), 4.1, undefined],
      [Date.UTC(2017, 10, 29, 1, 0, 0), 3.2, undefined],
      [Date.UTC(2017, 10, 29, 1, 30, 0), 4.2, undefined],
      [Date.UTC(2017, 10, 29, 2, 0, 0), 5, 5],
      [Date.UTC(2017, 10, 29, 2, 30, 0), 6, undefined],
      [Date.UTC(2017, 10, 29, 3, 0, 0), undefined, 7],
      [Date.UTC(2017, 10, 29, 4, 0, 0), undefined, 8],
      [Date.UTC(2017, 11, 5, 1, 0, 0), undefined, 9],
      [Date.UTC(2017, 11, 5, 1, 0, 0), undefined, 10]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #6 in C#
  it("'merge' test 6", function () {
    var data = [
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 30, 00), 1],
          [Date.UTC(2017, 11, 05, 00, 45, 00), 2],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.1],
          [Date.UTC(2017, 11, 05, 01, 15, 00), 4.1],
          [Date.UTC(2017, 11, 05, 01, 30, 00), 5.1],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.2],
          [Date.UTC(2017, 11, 05, 01, 15, 00), 4.2],
          [Date.UTC(2017, 11, 05, 01, 30, 00), 5.5],
          [Date.UTC(2017, 11, 05, 01, 45, 00), 6],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
        ]
      },
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 00, 00), 0],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.1],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.2],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 05, 03, 00, 00), 8],
        ]
      }
    ];
    var expected = [
      [Date.UTC(2017, 11, 5, 0, 0, 0), undefined, 0],
      [Date.UTC(2017, 11, 5, 0, 30, 0), 1, undefined],
      [Date.UTC(2017, 11, 5, 0, 45, 0), 2, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), 3.1, 3.1],
      [Date.UTC(2017, 11, 5, 1, 15, 0), 4.1, undefined],
      [Date.UTC(2017, 11, 5, 1, 30, 0), 5.1, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), 3.2, 3.2],
      [Date.UTC(2017, 11, 5, 1, 15, 0), 4.2, undefined],
      [Date.UTC(2017, 11, 5, 1, 30, 0), 5.5, undefined],
      [Date.UTC(2017, 11, 5, 1, 45, 0), 6, undefined],
      [Date.UTC(2017, 11, 5, 2, 0, 0), 7, 7],
      [Date.UTC(2017, 11, 5, 3, 0, 0), undefined, 8]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #7 in C#
  it("'merge' test 7", function () {
    var data = [
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 30, 00), 1],
          [Date.UTC(2017, 11, 05, 00, 45, 00), 2],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3],
          [Date.UTC(2017, 11, 05, 01, 15, 00), 4],
          [Date.UTC(2017, 11, 05, 01, 30, 00), 5],
          [Date.UTC(2017, 11, 05, 01, 45, 00), 6],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7]
        ]
      },
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 00, 00), 0],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.1],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.2],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 05, 03, 00, 00), 8]
        ]
      }
    ];
    var expected = [
      [Date.UTC(2017, 11, 5, 0, 0, 0), undefined, 0],
      [Date.UTC(2017, 11, 5, 0, 30, 0), 1, undefined],
      [Date.UTC(2017, 11, 5, 0, 45, 0), 2, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), 3, 3.1],
      [Date.UTC(2017, 11, 5, 1, 15, 0), 4, undefined],
      [Date.UTC(2017, 11, 5, 1, 30, 0), 5, undefined],
      [Date.UTC(2017, 11, 5, 1, 45, 0), 6, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), undefined, 3.2],
      [Date.UTC(2017, 11, 5, 2, 0, 0), 7, 7],
      [Date.UTC(2017, 11, 5, 3, 0, 0), undefined, 8]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #8 in C#
  it("'merge' test 8", function () {
    var data = [
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 30, 00), 1],
          [Date.UTC(2017, 11, 05, 00, 45, 00), 2],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3],
          [Date.UTC(2017, 11, 05, 01, 15, 00), 4],
          [Date.UTC(2017, 11, 05, 01, 30, 00), 5],
          [Date.UTC(2017, 11, 05, 01, 45, 00), 6],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 06, 01, 00, 00), 9]
        ]
      },
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 00, 00), 0],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.1],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.2],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 05, 03, 00, 00), 8],
          [Date.UTC(2017, 11, 06, 01, 00, 00), 9]
        ]
      }
    ];
    var expected = [
      [Date.UTC(2017, 11, 5, 0, 0, 0), undefined, 0],
      [Date.UTC(2017, 11, 5, 0, 30, 0), 1, undefined],
      [Date.UTC(2017, 11, 5, 0, 45, 0), 2, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), 3, 3.1],
      [Date.UTC(2017, 11, 5, 1, 15, 0), 4, undefined],
      [Date.UTC(2017, 11, 5, 1, 30, 0), 5, undefined],
      [Date.UTC(2017, 11, 5, 1, 45, 0), 6, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), undefined, 3.2],
      [Date.UTC(2017, 11, 5, 2, 0, 0), 7, 7],
      [Date.UTC(2017, 11, 5, 3, 0, 0), undefined, 8],
      [Date.UTC(2017, 11, 6, 1, 0, 0), 9, 9]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #9 in C#
  it("'merge' test 9", function () {
    var data = [
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 30, 00), 1],
          [Date.UTC(2017, 11, 05, 00, 45, 00), 2],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3],
          [Date.UTC(2017, 11, 05, 01, 15, 00), 4],
          [Date.UTC(2017, 11, 05, 01, 30, 00), 5],
          [Date.UTC(2017, 11, 05, 01, 45, 00), 6],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 06, 01, 00, 00), 9]
        ]
      },
      {
        points: []
      },
      {
        points: [
          [Date.UTC(2017, 11, 05, 00, 00, 00), 0],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.1],
          [Date.UTC(2017, 11, 05, 01, 00, 00), 3.2],
          [Date.UTC(2017, 11, 05, 02, 00, 00), 7],
          [Date.UTC(2017, 11, 05, 03, 00, 00), 8],
          [Date.UTC(2017, 11, 06, 01, 00, 00), 9]
        ]
      }
    ];
    var expected = [
      [Date.UTC(2017, 11, 5, 0, 0, 0), undefined, undefined, 0],
      [Date.UTC(2017, 11, 5, 0, 30, 0), 1, undefined, undefined],
      [Date.UTC(2017, 11, 5, 0, 45, 0), 2, undefined, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), 3, undefined, 3.1],
      [Date.UTC(2017, 11, 5, 1, 15, 0), 4, undefined, undefined],
      [Date.UTC(2017, 11, 5, 1, 30, 0), 5, undefined, undefined],
      [Date.UTC(2017, 11, 5, 1, 45, 0), 6, undefined, undefined],
      [Date.UTC(2017, 11, 5, 1, 0, 0), undefined, undefined, 3.2],
      [Date.UTC(2017, 11, 5, 2, 0, 0), 7, undefined, 7],
      [Date.UTC(2017, 11, 5, 3, 0, 0), undefined, undefined, 8],
      [Date.UTC(2017, 11, 6, 1, 0, 0), 9, undefined, 9]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

  // #10 in C#
  it("'merge' test 10", function () {
    var data = [
      {
        points: [
          [-62135596800000, 4.844023070703587],
          [1431907200000, 49.06573],
          [1461110400000, 4.764132],
          [1485043200000, 2.965715],
          [1492646400000, 5.061024],
          [1524182400000, 4.716382],
          [1552953600000, 4.27668240863892],
          [1554076800000, 5.45905382767612],
          [1560902400000, 5.459053827676122],
          [1571788800000, 5.12116663305445],
          [1575849600000, 10.165575671190226],
          [1586822400000, 4.370118285731856],
          [1623715200000, 4.844023070703587]
        ]
      },
      {
        points: [
          [1524196800000, 4.716382],
          [1554091200000, 5.459053827676122],
          [1560916800000, 5.459053827676122],
          [1571803200000, 5.121166633054448],
          [1575867600000, 5.121166633054448]
        ]
      }
    ];
    var expected = [
      [-62135596800000, 4.844023070703587, undefined],
      [1431907200000, 49.06573, undefined],
      [1461110400000, 4.764132, undefined],
      [1485043200000, 2.965715, undefined],
      [1492646400000, 5.061024, undefined],
      [1524182400000, 4.716382, undefined],
      [1524196800000, undefined, 4.716382],
      [1552953600000, 4.27668240863892, undefined],
      [1554076800000, 5.45905382767612, undefined],
      [1554091200000, undefined, 5.459053827676122],
      [1560902400000, 5.459053827676122, undefined],
      [1560916800000, undefined, 5.459053827676122],
      [1571788800000, 5.12116663305445, undefined],
      [1571803200000, undefined, 5.121166633054448],
      [1575849600000, 10.165575671190226, undefined],
      [1575867600000, undefined, 5.121166633054448],
      [1586822400000, 4.370118285731856, undefined],
      [1623715200000, 4.844023070703587, undefined]
    ];
    var reverse = false;

    // act
    var result = ShoojuClient.helpers.merge(data, reverse);

    // assert
    expect(result).toEqual(expected);
  });

});
