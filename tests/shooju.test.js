describe("ShoojuClient", function () {
  var server;
  var shoojuClient;
  var api_username;
  var api_secret;
  var api_server;
  var ignore_sjts;
  var options;

  var createClient = function (rawParams) {
    if (rawParams) {
      shoojuClient = new ShoojuClient(api_server, api_username, api_secret, ignore_sjts);
    } else {
      options = {
        api_server: api_server,
        ignore_sjts: ignore_sjts,
        get_credentials: function () {
          return {
            api_username: api_username,
            api_secret: api_secret
          }
        }
      };
      shoojuClient = new ShoojuClient(options);
    }
  };

  beforeEach(function () {
    api_username = 'api_username';
    api_secret = 'api_secret';
    api_server = 'api_server';
    ignore_sjts = true;
    server = sinon.fakeServer.create();
  });

  afterEach(function () {
    shoojuClient = null;
    server.restore();
  });

  it("is defined", function () {
    expect(typeof ShoojuClient).toBe('function');
  });

  it("is created and raw params passed correctly", function () {
    createClient(true);

    expect(shoojuClient.options.api_server).toBe(api_server);
    expect(shoojuClient.options.ignore_sjts).toBe(ignore_sjts);
    expect(shoojuClient.options.get_credentials().api_username).toBe(api_username);
    expect(shoojuClient.options.get_credentials().api_secret).toBe(api_secret);
  });

  it("is created and options params passed correctly", function () {
    createClient();

    expect(shoojuClient.options).toBe(options);
    expect(shoojuClient.options.api_server).toBe(api_server);
    expect(shoojuClient.options.ignore_sjts).toBe(ignore_sjts);
    expect(shoojuClient.options.get_credentials().api_username).toBe(api_username);
    expect(shoojuClient.options.get_credentials().api_secret).toBe(api_secret);
  });

  it("all public members are defined", function () {
    createClient();

    expect(typeof shoojuClient.request).toBe('function');
    expect(typeof shoojuClient.download).toBe('function');
    expect(typeof shoojuClient.ws).toBe('function');
    expect(typeof shoojuClient.raw.get).toBe('function');
    expect(typeof shoojuClient.raw.post).toBe('function');
  });

  it("'raw.get' makes GET request", function () {
    createClient();

    var successCallback = sinon.fake();
    var errorCallback = sinon.fake();
    sinon.spy(shoojuClient, 'request');

    // act
    var args = { a: 1 };
    var xhr = shoojuClient.raw.get('/series', args, successCallback, errorCallback);

    // assert
    expect(xhr instanceof XMLHttpRequest).toBeTruthy();
    expect(shoojuClient.request.getCall(0).args).toEqual(['/series', 'GET', args, null, successCallback, errorCallback]);
  });

  it("'raw.post' makes POST request", function () {
    createClient();

    var successCallback = sinon.fake();
    var errorCallback = sinon.fake();
    sinon.spy(shoojuClient, 'request');

    // act
    var args = { a: 1 };
    var body = { b: 2 };
    var xhr = shoojuClient.raw.post('/series', args, body, successCallback, errorCallback);

    // assert
    expect(xhr instanceof XMLHttpRequest).toBeTruthy();
    expect(shoojuClient.request.getCall(0).args).toEqual(['/series', 'POST', args, body, successCallback, errorCallback]);
  });

  it("'ws' creates ws connection", function () {

    var dummySocket = { send : sinon.spy() };
    sinon.stub(window, 'WebSocket').returns(dummySocket);

    createClient();

    // act
    var ws = shoojuClient.ws();

    // assert
    expect(typeof ws.subscribe == 'function').toBeTruthy();
    expect(typeof ws.close == 'function').toBeTruthy();
  });

  describe("'request' JSON call", function () {
    it("called success callback", function () {
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();

      // act
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});
      server.requests[0].respond(200, {}, JSON.stringify({}));

      // assert
      expect(xhr instanceof XMLHttpRequest).toBeTruthy();
      expect(successCallback.calledOnce).toBeTruthy();
      expect(errorCallback.notCalled).toBeTruthy();
    });

    it("called fail callback", function () {
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});
      server.requests[0].respond(500, {}, JSON.stringify({}));

      // assert
      expect(xhr instanceof XMLHttpRequest).toBeTruthy();
      expect(successCallback.notCalled).toBeTruthy();
      expect(errorCallback.calledOnce).toBeTruthy();
    });

    it("set shooju headers and all params", function () {
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();

      // run
      var args = {
        a: 1,
        b: true,
        c: 'str',
        d: null,
        e: undefined
      };
      var xhr = shoojuClient.request('/series', 'POST', args, args, successCallback, errorCallback, {});

      // assert
      expect(server.requests[0].method).toBe('POST');
      expect(server.requests[0].url).toBe('api_server/api/1/series?a=1&b=true&c=str&d=null&e=undefined&location_type=js');
      expect(server.requests[0].requestBody).toBe('{"a":1,"b":true,"c":"str","d":null,"location_type":"js"}');
      expect(server.requests[0].requestHeaders['Authorization']).toBe('Basic YXBpX3VzZXJuYW1lOmFwaV9zZWNyZXQ=');
      expect(server.requests[0].requestHeaders['Content-Type']).toBe('text/plain;charset=utf-8');
    });
  });

  describe("'request' SJTS call", function () {

    it("called success callback", function () {
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});
      server.requests[0].respond(200, {}, JSON.stringify({}));

      // assert
      expect(xhr instanceof XMLHttpRequest).toBeTruthy();
      expect(successCallback.calledOnce).toBeTruthy();
      expect(errorCallback.notCalled).toBeTruthy();
    });

    it("called fail callback", function () {
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});
      server.requests[0].respond(500, {}, JSON.stringify({}));

      // assert
      expect(xhr instanceof XMLHttpRequest).toBeTruthy();
      expect(successCallback.notCalled).toBeTruthy();
      expect(errorCallback.calledOnce).toBeTruthy();
    });

    it("called POST /series", function () {
      ignore_sjts = false;
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();
      sinon.spy(shoojuClient, 'sjts_request');

      // run
      var xhr = shoojuClient.request('/series', 'POST', {}, {}, successCallback, errorCallback, {});

      // assert
      expect(shoojuClient.sjts_request.calledOnce).toBeTruthy();
      expect(server.requests[0].method).toBe('POST');
      expect(server.requests[0].url).toBe('api_server/api/1/series?location_type=js');
      expect(server.requests[0].requestBody).toBe('{}');
      expect(server.requests[0].requestHeaders['Authorization']).toBe('Basic YXBpX3VzZXJuYW1lOmFwaV9zZWNyZXQ=');
      expect(server.requests[0].requestHeaders['Content-Type']).toBe('text/plain;charset=utf-8');
      expect(server.requests[0].requestHeaders['Sj-Receive-Format']).toBe('sjts');
      expect(server.requests[0].responseType).toBe('arraybuffer');
    });

    it("called GET /series", function () {
      ignore_sjts = false;
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();
      sinon.spy(shoojuClient, 'sjts_request');

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});

      // assert
      expect(shoojuClient.sjts_request.calledOnce).toBeTruthy();
      expect(server.requests[0].method).toBe('GET');
      expect(server.requests[0].url).toBe('api_server/api/1/series?location_type=js');
      expect(server.requests[0].requestBody).toBe(null);
      expect(server.requests[0].requestHeaders['Authorization']).toBe('Basic YXBpX3VzZXJuYW1lOmFwaV9zZWNyZXQ=');
      expect(server.requests[0].requestHeaders['Content-Type']).toBe('text/plain;charset=utf-8');
      expect(server.requests[0].requestHeaders['Sj-Receive-Format']).toBe('sjts');
      expect(server.requests[0].responseType).toBe('arraybuffer');
    });

    it("ignored for /jobs", function () {
      ignore_sjts = false;
      createClient();

      var successCallback = sinon.fake();
      var errorCallback = sinon.fake();
      sinon.spy(shoojuClient, 'sjts_request');

      // run
      var xhr = shoojuClient.request('/jobs', 'POST', {}, {}, successCallback, errorCallback, {});

      // assert
      expect(shoojuClient.sjts_request.notCalled).toBeTruthy();
      expect(server.requests[0].method).toBe('POST');
      expect(server.requests[0].url).toBe('api_server/api/1/jobs?location_type=js');
      expect(server.requests[0].requestBody).toBe('{}');
      expect(server.requests[0].requestHeaders['Authorization']).toBe('Basic YXBpX3VzZXJuYW1lOmFwaV9zZWNyZXQ=');
      expect(server.requests[0].requestHeaders['Content-Type']).toBe('text/plain;charset=utf-8');
      expect(server.requests[0].requestHeaders['Sj-Receive-Format']).toBeUndefined();
      expect(server.requests[0].responseType).toBe('text');
    });

    it("parse response", function () {
      ignore_sjts = false;
      createClient();

      var successCallback = sinon.spy();
      var errorCallback = sinon.spy();

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});

      var expectedResponse = '{"points_desc":false,"request_id":"MrMXNXw6WjdugsR","series":[{"fields":{"description":"my first desc"},"points":[[1179273600000,0.8530213091685954],[1179360000000,0.3947887232059766],[1179446400000,0.6573902189436704]],"series_id":"test\\\\1","stats":{"avg":0.6350667504,"max":0.8530213092,"min":0.3947887232,"std":0.1877374561}}],"success":true,"total":1}';
      var base64Arraybuffer = "U0pUU1gAAAB7InBvaW50c19kZXNjIjpmYWxzZSwicmVxdWVzdF9pZCI6Ik1yTVhOWHc2V2pkdWdzUiIsInNlcmllcyI6MSwic3VjY2VzcyI6dHJ1ZSwidG90YWwiOjF9oQAAAHsiZmllbGRzIjp7ImRlc2NyaXB0aW9uIjoibXkgZmlyc3QgZGVzYyJ9LCJwb2ludHMiOjMsInNlcmllc19pZCI6InRlc3RcXDEiLCJzdGF0cyI6eyJhdmciOjAuNjM1MDY2NzUwNCwibWF4IjowLjg1MzAyMTMwOTIsIm1pbiI6MC4zOTQ3ODg3MjMyLCJzdGQiOjAuMTg3NzM3NDU2MX19JAAAAAAAAAAAHC+SEgEAAABcJgV2cjVY80vrP/jzv+s3RNk/1lhiNlcJ5T8=";
      var response = Uint8Array.from(atob(base64Arraybuffer), function (c) {
        return c.charCodeAt(0);
      }).buffer;

      server.requests[0].respond(200, {}, response);

      // assert
      expect(successCallback.calledOnce).toBeTruthy();
      expect(errorCallback.notCalled).toBeTruthy();

      var response = successCallback.firstCall.args[0];
      expect(JSON.stringify(response)).toBe(expectedResponse);
    });

    it("detect bad response", function () {
      ignore_sjts = false;
      createClient();

      var successCallback = sinon.spy();
      var errorCallback = sinon.spy();

      // run
      var xhr = shoojuClient.request('/series', 'GET', {}, null, successCallback, errorCallback, {});

      var base64Arraybuffer = "c2Rmc2Y=";
      var response = Uint8Array.from(atob(base64Arraybuffer), function (c) {
        return c.charCodeAt(0);
      }).buffer;

      var error;
      var respond = function () {
        try {
          server.requests[0].respond(200, {}, response);
        } catch (e) {
          error = e;
          throw e;
        }
      }

      // assert
      expect(respond).toThrow();
      expect(error.message).toContain('First 4 bytes should be `SJTS` string...', 'Wrong error message');
      expect(successCallback.notCalled).toBeTruthy('Success callback was called');
      expect(errorCallback.calledOnce).toBeTruthy('Error callback was not called once');
    });

  });

});
