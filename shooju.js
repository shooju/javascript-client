// fallbacks for node
if (typeof global !== 'undefined') {
    if (!global.XMLHttpRequest) {
        global.XMLHttpRequest = require("xhr2");
    }
    if (!global.btoa) {
        global.btoa = function (str) {
            return Buffer.from(str).toString('base64');
        }
    }
}


/*
 Shooju API client
*/
; (function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            global.ShoojuClient = factory()
}(this, (function () {
    'use strict';

    //     Int64.js
    //
    //     Copyright (c) 2012 Robert Kieffer
    //     MIT License - http://opensource.org/licenses/mit-license.php

    /**
     * Support for handling 64-bit int numbers in Javascript (node.js)
     *
     * JS Numbers are IEEE-754 binary double-precision floats, which limits the
     * range of values that can be represented with integer precision to:
     *
     * 2^^53 <= N <= 2^53
     *
     * Int64 objects wrap a node Buffer that holds the 8-bytes of int64 data.  These
     * objects operate directly on the buffer which means that if they are created
     * using an existing buffer then setting the value will modify the Buffer, and
     * vice-versa.
     *
     * Internal Representation
     *
     * The internal buffer format is Big Endian.  I.e. the most-significant byte is
     * at buffer[0], the least-significant at buffer[7].  For the purposes of
     * converting to/from JS native numbers, the value is assumed to be a signed
     * integer stored in 2's complement form.
     *
     * For details about IEEE-754 see:
     * http://en.wikipedia.org/wiki/Double_precision_floating-point_format
     */

    //
    // Int64
    //

    /**
     * Constructor accepts any of the following argument types:
     *
     * new Int64(buffer[, offset=0]) - Existing Buffer with byte offset
     * new Int64(string)             - Hex string (throws if n is outside int64 range)
     * new Int64(number)             - Number (throws if n is outside int64 range)
     * new Int64(hi, lo)             - Raw bits as two 32-bit values
     */

    var Int64 = function (a1, offset) {
        offset = offset || 0;
        if (a1 instanceof Array) {
            this.storage = a1.slice(offset, 8);
        } else {
            this.storage = this.storage || new Array(8);
            this.setValue.apply(this, arguments);
        }
    };


    // Max integer value that JS can accurately represent
    Int64.MAX_INT = Math.pow(2, 53);

    // Min integer value that JS can accurately represent
    Int64.MIN_INT = -Math.pow(2, 53);

    Int64.HexTable = new Array(256);
    for (var i = 0; i < 256; i++) {
        Int64.HexTable[i] = (i > 0xF ? '' : '0') + i.toString(16);
    }

    Int64.prototype = {
        /**
         * Do in-place 2's compliment.  See
         * http://en.wikipedia.org/wiki/Two's_complement
         */
        _2scomp: function () {
            var b = this.storage, o = o, carry = 1;
            for (var i = o + 7; i >= o; i--) {
                var v = (b[i] ^ 0xff) + carry;
                b[i] = v & 0xff;
                carry = v >> 8;
            }
        },

        /**
         * Set the value. Takes any of the following arguments:
         *
         * setValue(string) - A hexidecimal string
         * setValue(number) - Number (throws if n is outside int64 range)
         * setValue(hi, lo) - Raw bits as two 32-bit values
         */
        setValue: function (hi, lo) {
            var negate = false;
            if (arguments.length == 1) {
                if (typeof (hi) == 'number') {
                    // Simplify bitfield retrieval by using abs() value.  We restore sign
                    // later
                    negate = hi < 0;
                    hi = Math.abs(hi);
                    lo = hi % 0x80000000;
                    hi = hi / 0x80000000;
                    if (hi > 0x80000000) throw new RangeError(hi + ' is outside Int64 range');
                    hi = hi | 0;
                } else if (typeof (hi) == 'string') {
                    hi = (hi + '').replace(/^0x/, '');
                    lo = hi.substr(-8);
                    hi = hi.length > 8 ? hi.substr(0, hi.length - 8) : '';
                    hi = parseInt(hi, 16);
                    lo = parseInt(lo, 16);
                } else {
                    throw new Error(hi + ' must be a Number or String');
                }
            }

            // Technically we should throw if hi or lo is outside int32 range here, but
            // it's not worth the effort. Anything past the 32'nd bit is ignored.

            // Copy bytes to buffer
            var b = this.storage, o = 0;
            for (var i = 7; i >= 0; i--) {
                b[o + i] = lo & 0xff;
                lo = i == 4 ? hi : lo >>> 8;
            }

            // Restore sign of passed argument
            if (negate) this._2scomp();
        },

        /**
         * Convert to a native JS number.
         *
         * WARNING: Do not expect this value to be accurate to integer precision for
         * large (positive or negative) numbers!
         *
         * @param allowImprecise If true, no check is performed to verify the
         * returned value is accurate to integer precision.  If false, imprecise
         * numbers (very large positive or negative numbers) will be forced to +/-
         * Infinity.
         */
        toNumber: function (allowImprecise) {
            var b = this.storage, o = 0;

            // Running sum of octets, doing a 2's complement
            var negate = b[0] & 0x80, x = 0, carry = 1;
            for (var i = 7, m = 1; i >= 0; i--, m *= 256) {
                var v = b[o + i];

                // 2's complement for negative numbers
                if (negate) {
                    v = (v ^ 0xff) + carry;
                    carry = v >> 8;
                    v = v & 0xff;
                }

                x += v * m;
            }

            // Return Infinity if we've lost integer precision
            if (!allowImprecise && x >= Int64.MAX_INT) {
                return negate ? -Infinity : Infinity;
            }

            return negate ? -x : x;
        },

        /**
         * Convert to a JS Number. Returns +/-Infinity for values that can't be
         * represented to integer precision.
         */
        valueOf: function () {
            return this.toNumber(false);
        },

        /**
         * Return string value
         *
         * @param radix Just like Number#toString()'s radix
         */
        toString: function (radix) {
            return this.valueOf().toString(radix || 10);
        },

        /**
         * Return a string showing the buffer octets, with MSB on the left.
         *
         * @param sep separator string. default is '' (empty string)
         */
        toOctetString: function (sep) {
            var out = new Array(8);
            var b = this.storage, o = 0;
            for (var i = 0; i < 8; i++) {
                out[i] = Int64.HexTable[b[o + i]];
            }
            return out.join(sep || '');
        }
    };

    var helpers = {
        parseQueryAndOperators: function (query) {
            if (!query) {
                return {
                    q: query,
                    op: null
                }
            }

            query = query || '';

            var res = {
                q: '',
                op: null
            };

            if (query.indexOf('@') != -1) {
                var expRes = new RegExp(/(^.+?)(@[^{}'"]*)$/g).exec(query); //SPLIT_OPERATORS_REGEX in https://bitbucket.org/shooju/sj-toolkit
                if (expRes) {
                    res.q = expRes[1];
                    res.op = expRes[2];
                } else {
                    res.q = query;
                }
            } else {
                res.q = query;
            }

            return res;
        },
        merge: function (seriesList, reverseOrder) {
            var comparer = function (x, y) {
                if (x === y) {
                    return 0;
                }
                if (reverseOrder) {
                    return y > x ? 1 : -1;
                }
                return x > y ? 1 : -1;
            };

            seriesList.forEach(function (series) {
                if (series.points == null || !series.points.length) {
                    return;
                }
                if (comparer(series.points[0][0], series.points[series.points.length - 1][0]) > 0) {
                    throw new Error('Bad point dates');
                }
            });

            var indexes = seriesList.map(function (series) { return 0; });

            var dstIndexes = this._findDstIndexes(seriesList, indexes, reverseOrder, comparer);
            var dstActive = !!dstIndexes.filter(function (cur) { return cur >= 0; }).length;

            var res = [];
            var maxDate = reverseOrder ? -62135596800001 : 253402300800000; // "0001-01-01T00:00:00.000Z" and "9999-12-31T23:59:59.999Z"

            while (true) {
                if (dstActive) {
                    var dstComplete = true;

                    for (var i = 0; i < seriesList.length; i++) {
                        if (indexes[i] < dstIndexes[i]) {
                            dstComplete = false;
                            break;
                        }
                    }

                    if (dstComplete) {
                        dstIndexes = this._findDstIndexes(seriesList, indexes, reverseOrder, comparer);
                        dstActive = !!dstIndexes.filter(function (cur) {
                            return cur >= 0;
                        }).length;
                    }
                }

                var curDate = maxDate;

                this._iterateThroughSeries(seriesList, indexes, dstIndexes, function (point, i) {
                    if (comparer(point[0], curDate) < 0) {
                        curDate = point[0];
                    }
                });

                if (curDate == maxDate) {
                    break;
                }

                var line = new Array(seriesList.length + 1);
                line[0] = curDate;

                this._iterateThroughSeries(seriesList, indexes, dstIndexes, function (point, i) {
                    if (comparer(point[0], curDate) > 0) {
                        return;
                    }

                    line[i + 1] = point[1];
                    indexes[i]++;
                });

                res.push(line);
            }

            return res;
        },

        _findDstIndexes: function (seriesList, indexes, reverseOrder, comparer) {
            var res = new Array(seriesList.length);

            var minDate = reverseOrder ? 253402300800000 : -62135596800001; // "9999-12-31T23:59:59.999Z" and "0001-01-01T00:00:00.000Z"
            var curDate = null;

            var i = 0;
            seriesList.forEach(function (series) {
                if (!series.points) {
                    return;
                }

                var prevDate = minDate;

                for (var j = indexes[i]; j < series.points.length; j++) {
                    var point = series.points[j];

                    if (comparer(point[0], prevDate) <= 0 && (curDate === null || comparer(point[0], curDate) == 0)) {
                        curDate = point[0];
                        res[i] = j;
                        break;
                    }

                    prevDate = point[0];
                }

                i++;
            });

            if (curDate === null) {
                return res;
            }

            i = 0;
            seriesList.forEach(function (series) {
                if (!series.points) {
                    return;
                }
                if (res[i] >= 0) {
                    return;
                }

                for (var j = indexes[i]; j < series.points.length; j++) {
                    var point = series.points[j];

                    var timeDiff = point[0] - curDate;
                    if (reverseOrder) {
                        timeDiff = -timeDiff;
                    }

                    if (timeDiff >= 3600000) { // 1h
                        res[i] = j;
                        break;
                    }
                }

                i++;
            });

            return res;
        },

        _iterateThroughSeries: function (seriesList, indexes, dstIndexes, action) {
            seriesList.forEach(function (series, i) {
                if (series.points) {
                    var index = indexes[i];
                    if (index >= series.points.length) {
                        return;
                    }
                    if (indexes[i] == dstIndexes[i]) {
                        return;
                    }
                    var point = series.points[index];
                    action(point, i);
                }
            });
        }
    }

    /**
     * {
     *  api_server: string;
     *  api_username: string;
     *  api_secret: string;
     *  ignore_sjts?: string;
     *  get_credentials?: () => {
     *    api_username: string,
     *    api_secret: string
     *  }
     * }
     */
    var ShoojuClient = function (options) {
        var self = this;

        if (typeof (arguments[0]) != 'object') {
            var api_username = arguments[1];
            var api_secret = arguments[2];
            options = {
                api_server: arguments[0],
                ignore_sjts: arguments[3],
                get_credentials: function () {
                    return {
                        api_username: api_username,
                        api_secret: api_secret
                    };
                }
            }
        }

        if (options.api_server == "/") {
            options.api_server = '';
        }

        this.async = true;
        this.jsonpinmsie = true;

        this.options = options;

        this._prepareUrlArgs = function (url_args, addLocationParams) {
            if (!url_args) {
                url_args = {};
            }
            if (url_args && typeof (url_args) === 'object') {
                Object.keys(url_args).forEach(function (name) {
                    var value = url_args[name];
                    if (name.indexOf(':') == 0) {
                        url = url.replace(name, value);
                        delete url_args[name];
                    }
                });

                if (this._locationType !== null && addLocationParams) {
                    url_args["location_type"] = this._locationType || 'js';
                    if (this._location) {
                        url_args["location"] = this._location;
                    }
                }

                url_args = Object.keys(url_args).map(function (p) {
                    var v = url_args[p];
                    if (v && typeof v == 'object') {
                        v = JSON.stringify(v)
                    }
                    return p + "=" + encodeURIComponent(v);
                }).join('&');
            }
            return url_args;
        };

        // Base request method
        /**
         * options: {
         *  serialize?: false - don't serialize input data (for FormData)
         *  responseType: string - XMLHttpRequest response type
         *  baseUrl: string - third-party domain
         *  ignoreAuth?: true - don't add authorization header
         *  ignoreSpeed?: true - don't calculate network speed
         * }
         */
        this.request = function (url, method, url_args, data, success_callback, error_callback, options) {
            method = method.toUpperCase();


            // add new line to expression to correcly identify (query or expression) line where exception was thrown
            if (url_args && url_args.g_expression) {
                url_args.g_expression = '\n' + url_args.g_expression;
            }
            if (data && data.g_expression) {
                data.g_expression = '\n' + data.g_expression;
            }

            var sjForceJsonRequest = false;
            if (typeof window !== 'undefined') {
                sjForceJsonRequest = window._sjForceJsonRequest;
            }
            var enableSJTS = !sjForceJsonRequest && !self.options.ignore_sjts && url.toLowerCase() == '/series' && (method == 'GET' || method == 'POST');

            if (enableSJTS) {
                var date_format = (url_args && url_args.date_format) || (data && data.date_format) || 'milli';
                var diff_job_id = (url_args && url_args.diff_job_id) || (data && data.diff_job_id);

                if (date_format == 'milli' && !diff_job_id) {
                    return this.sjts_request(url, url_args, method, data, success_callback, error_callback, options);
                }
            }

            var baseUrl = self.options.api_server + '/api/1';
            var addLocationParams = true;
            if (options && options.baseUrl) {
                if (baseUrl != options.baseUrl) {
                    addLocationParams = false;
                }
                baseUrl = options.baseUrl;
            }

            var url_params = this._prepareUrlArgs(url_args, addLocationParams);

            var navigator;
            if (typeof window !== 'undefined') {
                navigator = window.navigator;
            }
            var responseType = navigator && self.jsonpinmsie && /msie/.test(navigator.userAgent.toLowerCase()) ? "jsonp" : "text";
            if (method == 'GET') {
                data = url_params;
            } else { //data is payload
                if (typeof (data) == "object" && (!options || options.serialize !== false)) { // data can be FormData which should be send as is
                    data = JSON.stringify(data);
                }
            }

            if (options) {
                if (options.responseType) {
                    responseType = options.responseType;
                }
            }

            // -------------- BUILD REQUEST ---------------
            var xhr = new XMLHttpRequest();

            xhr.open(method, baseUrl + url + (url_params == null ? '' : "?" + url_params), self.async);

            var credentials = self.options.get_credentials();

            if (!options || options.ignoreAuth !== true) {
                if (credentials.api_username != '' && credentials.api_username != null) {
                    var auth = "Basic " + btoa(credentials.api_username + ":" + credentials.api_secret);
                    if ((options && options.authHeader) || credentials.sjauth) {
                        xhr.setRequestHeader("Sj-Auth", auth);
                    } else {
                        xhr.setRequestHeader("Authorization", auth);
                    }
                }
            }

            xhr.responseType = responseType;

            if (options && options.headers && typeof options.headers == 'object') {
                Object.keys(options.headers).forEach(function (headerKey) {
                    xhr.setRequestHeader(headerKey, options.headers[headerKey]);
                });
            }

            if (options && options.contentType) {
                xhr.setRequestHeader("Content-Type", options.contentType);
            }

            if (options && options.uploadOnProgress && xhr.upload) {
                xhr.upload.onprogress = options.uploadOnProgress;
            }

            xhr.onreadystatechange = function () { // (3)
                if (xhr.readyState != 4) return;
                var response, responseText;

                var contentType = xhr.getResponseHeader('Content-Type');
                var isJsonRequest = !responseType || responseType == 'text';
                if (isJsonRequest || contentType == 'application/json') {
                    if (responseType == 'arraybuffer') { // arraybuffer request may return json with error
                        var arr = new Uint8Array(xhr.response);
                        responseText = String.fromCharCode.apply(String, arr);
                    } else {
                        responseText = xhr.responseText;
                    }
                    response = responseText;
                    var networkSpeed;
                    if (!options || options.ignoreSpeed !== true) {
                        networkSpeed = self.getNetworkSpeed(xhr, startTime);
                    }
                    try {
                        response = JSON.parse(responseText);
                    } catch (e) { }
                } else {
                    response = xhr.response;
                }

                if (xhr.status == 200) {
                    if (success_callback) {
                        processResponse(url, url_args, method, data, response);
                        success_callback(response, networkSpeed);
                    }
                } else {
                    if (error_callback) {
                        error_callback(response, networkSpeed);
                    }
                }
            }
            var startTime = new Date().getTime();
            xhr.send(data);
            return xhr;
        };

        /**
         * Download file
         * url - request uri
         * method - http method
         * url_args - object with query params
         * data - object for request body  (for POST/PUT/...)
         * success_callback
         * error_callback
         * fileName - string file name (used as fallback, only when api doesn't return file name) or function which accepts api file name and returns result file name
         */
        this.download = function (url, method, url_args, data, success_callback, error_callback, fileName, options) {
            var requestOptions = Object.assign({}, options || {}, {
                responseType: 'arraybuffer'
            });
            var request = this.request(url, method, url_args, data, function (response) {

                var m_file_size = requestOptions.max_file_size || max_file_size;

                // json with error may be returned, so prevent download file with json error
                var isArrayBuffer = response && response.byteLength !== undefined && response.success === undefined;
                if (!isArrayBuffer) {
                    if (error_callback) {
                        error_callback(response);
                    }
                    return;
                }

                if (response.byteLength > m_file_size) {
                    if (error_callback) {
                        error_callback({
                            success: false,
                            error: 'max_file_size',
                            description: 'File exceeded max file size.'
                        });
                    }
                    return;
                }

                // Use file name from response
                var disposition = request.getResponseHeader('Content-Disposition');
                var fName;
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) {
                        fName = matches[1].replace(/['"]/g, '');
                    }
                }

                if (fileName) {
                    if (typeof fileName == 'function') {
                        var newFileName = fileName(fName);
                        if (newFileName) {
                            fName = newFileName;
                        }
                    } else { // used as fallback, only when api doesn't return file name
                        if (!fName) {
                            fName = fileName;
                        }
                    }
                }

                var contentType = request.getResponseHeader('Content-Type');
                var maxSizeForBase64 = 1048576; //1024 * 1024
                var windowUrl = window.URL || window.webkitURL;
                if (navigator.msSaveBlob) { // IE 10+ 
                    var blob = new Blob([response], { type: contentType });
                    navigator.msSaveBlob(blob, fName);
                } else if (response.byteLength > maxSizeForBase64 && typeof windowUrl.createObjectURL === 'function') {
                    var blob = new Blob([response], { type: contentType });
                    var downloadUrl = windowUrl.createObjectURL(blob);
                    var a = document.createElement("a");
                    a.href = downloadUrl;
                    a.download = fName;
                    a.click();
                    windowUrl.revokeObjectURL(downloadUrl);
                }
                else {
                    var base64data = btoa(new Uint8Array(response).reduce(function (data, byte) { return data + String.fromCharCode(byte) }, ''));
                    downloadUrl = 'data:' + contentType + ';base64,' + base64data;
                    var a = document.createElement("a");
                    a.href = downloadUrl;
                    a.download = fName;
                    a.click();
                }
                if (success_callback)
                    success_callback();
            }, function (error) {
                if (error_callback)
                    error_callback(error);
            }, requestOptions);
            return request;
        };

        var chunks_file_min_size = 1073741824; // 1gb
        var chunk_size = 262144000; // 250mb
        var _initSession = function (file_name, success, error) {
            /*
            Shooju.ajax({
              type: 'POST',
              contentType: "application/json",
              data: JSON.stringify({ filename: file_name }),
              action:  'files/multipart/init',
              success: success,
              error: error
            });
            */
            return self.request('/files/multipart/init', 'POST', {
            }, {
                filename: file_name
            }, success, error);
        };
        var _uploadChunk = function (file_id, part_num, file_chunk, progress, success, error) {
            var formdata = new FormData();
            formdata.append('file', file_chunk);

            /*
            Shooju.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                progress: progress,
                data: formdata,
                action: 'files/multipart/upload?file_id=' + file_id + '&part_num=' + part_num,
                success: success,
                error: error
            });
            */

            return self.request('/files/multipart/upload', 'POST', {
                file_id: file_id,
                part_num: part_num
            }, formdata, success, error, {
                uploadOnProgress: progress,
                serialize: false
            });
        };
        var _uploadFile = function (file, progress, success, error) {
            var formdata = new FormData();
            formdata.append('file', file);
            /*
            Shooju.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                progress: progress,
                data: formdata,
                action: 'files/',
                success: success,
                error: error
            });
            */
            return self.request('/files', 'POST', {
            }, formdata, success, error, {
                uploadOnProgress: progress,
                serialize: false
            });
        };
        var _completeSession = function (file_id, success, error) {
            /*
            Shooju.ajax({
                type: 'POST',
                contentType: "application/json",
                action: 'files/multipart/complete?file_id=' + file_id,
                success: success,
                error: error
            });
            */
            return self.request('/files/multipart/complete', 'POST', {
                file_id: file_id
            }, {}, success, error);
        };

        var _batchChunksUpload = function (file_id, part_number, file, total_upload_size, total_uploaded_size, options, file_index) {
            return new Promise(function (resolve, reject) {
                var end_position = part_number * chunk_size;
                var start_position = end_position - chunk_size;
                var file_chunk;
                if (file.size < chunks_file_min_size) {
                    file_chunk = file;
                } else {
                    file_chunk = file.slice(start_position, end_position, file.type);
                }
                if (!file_chunk.size && part_number !== 1) {
                    resolve();
                    return;
                }

                var file_progress_start = 0;
                var file_progress_rate = 0;
                if (total_upload_size && total_uploaded_size) {
                    file_progress_start = total_uploaded_size / total_upload_size * 100;
                }
                if (file.size && total_upload_size) {
                    file_progress_rate = file.size / total_upload_size;
                }

                var chunk_progress_start = file_progress_start;
                var chunk_progress_rate = 0;
                if (start_position && file.size) {
                    chunk_progress_start = file_progress_start + start_position / file.size * 100 * file_progress_rate;
                }
                if (file_chunk.size && file.size) {
                    chunk_progress_rate = file_chunk.size / file.size * file_progress_rate;
                }

                var progress = function (e) {
                    if (options.progress) {
                        var fileProgressPrc = e.loaded / e.total * 100 * chunk_progress_rate + chunk_progress_start;
                        options.progress(e, fileProgressPrc, file_index, file);
                    }
                };

                if (file.size < chunks_file_min_size) {
                    // single file
                    options.xhrs.push(_uploadFile(file_chunk, progress, function (x) {
                        resolve(x);
                    }, reject));
                } else {
                    // chunks
                    options.xhrs.push(_uploadChunk(file_id, part_number, file_chunk, progress, function () {
                        _batchChunksUpload(file_id, part_number + 1, file, total_upload_size, total_uploaded_size, options, file_index).then(resolve, reject);
                    }, reject));
                }
            });
        };

        // recursive upload files (upload file and call itself to upload next file)
        var _batchFilesUpload = function (files, file_index, total_upload_size, uploaded_files_data, total_uploaded_size, options) {
            if (!uploaded_files_data) {
                uploaded_files_data = [];
            }
            if (!total_uploaded_size) {
                total_uploaded_size = 0;
            }

            return new Promise(function (resolve, reject) {
                if (file_index >= files.length) {
                    resolve(uploaded_files_data);
                    return;
                }

                // `multipart` if `file_id` is passed
                var uploadFile = function (multipart_file_id) {
                    _batchChunksUpload(multipart_file_id, 1, files[file_index], total_upload_size, total_uploaded_size, options, file_index).then(function (response) {
                        var ready = function () {
                            uploaded_files_data[file_index] = {
                                file_id: multipart_file_id || (response.file_id ? response.file_id[0] : null),
                                size: files[file_index].size,
                                name: files[file_index].name,
                                response: response
                            };

                            // upload next file
                            _batchFilesUpload(files, file_index + 1, total_upload_size, uploaded_files_data, total_uploaded_size + files[file_index].size, options)
                                .then(resolve, reject);
                        };
                        if (multipart_file_id) {
                            options.xhrs.push(_completeSession(multipart_file_id, ready, reject));
                        } else {
                            ready();
                        }
                    }, reject);
                };

                if (files[file_index].size >= chunks_file_min_size) {
                    // chunks
                    options.xhrs.push(_initSession(files[file_index].name, function (initResp) {
                        uploadFile(initResp.file_id);
                    }, reject));
                } else {
                    // single file
                    uploadFile();
                }
            });

        };

        var max_file_size = 2147483648; // 2gb (SJP-5419)

        // recursive upload files (upload file and call itself to upload next file)
        this.filesUpload = function (files, success_callback, error_callback, options) {
            var total_upload_size = 0;

            options = options || {};
            options.xhrs = options.xhrs || [];

            var tooLargeFiles = [];
            var m_file_size = options.max_file_size || max_file_size;

            for (var i = 0; i < files.length; i++) {
                total_upload_size += files[i].size;
                if (files[i].size > m_file_size) {
                    tooLargeFiles.push(files[i]);
                }
            }

            if (tooLargeFiles.length) {
                if (error_callback) {
                    error_callback({
                        success: false,
                        error: 'max_file_size',
                        description: (tooLargeFiles.length > 1 ? (tooLargeFiles.length + ' files') : 'File') + ' exceeded max file size.',
                        files: tooLargeFiles
                    });
                }
                return;
            }

            _batchFilesUpload(files, 0, total_upload_size, undefined, undefined, options).then(success_callback, error_callback);
        };

        this.getNetworkSpeed = function (xhr, startTime) {
            var totalDurationMs = new Date().getTime() - startTime;
            var contentLength = xhr.getResponseHeader('Content-Length');
            var serverDurationSec = xhr.getResponseHeader('Sj-Duration');
            if (contentLength && serverDurationSec) {
                var transportDurationSec = totalDurationMs / 1000 - serverDurationSec;
                return contentLength / 1024 / 1024 / transportDurationSec;
            }
        };

        this.sjts_getString = function (bytes, offset, end) {
            var s = '';
            var i = offset;
            while (i < end) {
                var c = bytes.getUint8(i++);
                if (c > 127) {
                    if (c > 191 && c < 224) {
                        if (i >= end) throw 'UTF-8 decode: incomplete 2-byte sequence';
                        c = (c & 31) << 6 | bytes.getUint8(i) & 63;
                    } else if (c > 223 && c < 240) {
                        if (i + 1 >= end) throw 'UTF-8 decode: incomplete 3-byte sequence';
                        c = (c & 15) << 12 | (bytes.getUint8(i) & 63) << 6 | bytes.getUint8(++i) & 63;
                    } else if (c > 239 && c < 248) {
                        if (i + 2 >= end) throw 'UTF-8 decode: incomplete 4-byte sequence';
                        c = (c & 7) << 18 | (bytes.getUint8(i) & 63) << 12 | (bytes.getUint8(++i) & 63) << 6 | bytes.getUint8(++i) & 63;
                    } else throw 'UTF-8 decode: unknown multibyte start 0x' + c.toString(16) + ' at index ' + (i - 1);
                    ++i;
                }
                if (c === 0) break;
                if (c <= 0xffff) s += String.fromCharCode(c);
                else if (c <= 0x10ffff) {
                    c -= 0x10000;
                    s += String.fromCharCode(c >> 10 | 0xd800)
                    s += String.fromCharCode(c & 0x3FF | 0xdc00)
                } else throw 'UTF-8 decode: code point 0x' + c.toString(16) + ' exceeds UTF-16 reach';
            }
            return s;
        };

        var processResponse = function (url, url_args, method, post_data, response) {
            url = url || '';
            url_args = url_args || {};
            post_data = post_data || {};

            // line #1 - error in query
            // line #2+ - error in expression (because we always add \n at the beginning)
            // here parsing last line number from error and decreasing line numbers back to match original lines
            if (response && !response.success && response.error == 'expression' && response.description &&
                url == '/series' && (url_args.g_expression || post_data.g_expression)) {

                var errorStacktrace = response.description;

                var lastLn;
                var lastXprLn;
                try {
                    errorStacktrace = errorStacktrace.replace(/((\s|\()line(\s|\:))(\d+)/g, function () {
                        var ln = arguments[4] - 0;
                        lastLn = ln;
                        if (ln > 1) {
                            ln--;
                            lastXprLn = ln;
                        }
                        return arguments[1] + (ln + '');
                    });
                } catch (ex) {
                    if (typeof window !== 'undefined') {
                        console.warn('Unable to parse error description', ex);
                    }
                }

                if (typeof lastLn === 'number') {
                    response.description = errorStacktrace;
                    if (typeof lastXprLn === 'number') {
                        response._expression_error_line = lastXprLn;
                    }
                }
            }
        };

        this.sjts_parse = function (sjts, counter) {
            var dv, badSJTS;
            if (!sjts) {
                badSJTS = 'blank';
            } else if (sjts.byteLength < 4) {
                badSJTS = 'bad length: ' + sjts.byteLength + 'bytes';
            } else {
                dv = new DataView(sjts);
                var prefixNum = dv.getUint32(0, true);
                if (prefixNum !== 1398032979) { // 1398032979 === 'SJTS'
                    badSJTS = 'bad prefixnum: ' + prefixNum;
                }
            }

            if (badSJTS) {
                var badSJTSText;
                var requestId = '<>';
                try {
                    var decoder = new TextDecoder("utf-8");
                    badSJTSText = decoder.decode(sjts);
                    if (badSJTSText) {
                        try {
                            var parsedObj = JSON.parse(badSJTSText);
                            if (parsedObj) {
                                requestId = parsedObj.request_id ? ('<' + parsedObj.request_id + '>') : '<blank>';
                            }
                        } catch (e) {
                            requestId = '<invalid>';
                        }
                        if (badSJTSText.length > 200) {
                            badSJTSText = badSJTSText.substr(0, 200) + '...';
                        }
                        badSJTSText = requestId + '[' + counter + '] ' + badSJTSText;
                    }
                } catch (e) {
                    badSJTSText = 'Unable to decode bad SJTS[' + counter + ']: ' + (e || '');
                }

                var msg = 'First 4 bytes should be `SJTS` string... ' + badSJTS + '|' + (badSJTSText || '[blank]');
                if (typeof window !== 'undefined') {
                    console.log(msg);
                }
                throw new Error(msg);
            }

            var sz = dv.getUint32(4, true);
            var offs = 8;
            var rootJs = self.sjts_getString(dv, offs, offs + sz);
            var obj = JSON.parse(rootJs);
            var cnt = obj.series;
            obj.series = [];
            var extra_offs = offs + rootJs.length + 1;
            var sjts = obj['sjts']
            var includes_job = false;
            var includes_timestamp = false;
            if (sjts) {
                includes_job = sjts['includes_job'];
                includes_timestamp = sjts['includes_timestamp'];
                obj.sjts = undefined;
            }
            for (var i = 0; i < cnt; i++) {
                offs += sz;
                sz = dv.getUint32(offs, true);
                offs += 4;
                var js = self.sjts_getString(dv, offs, offs + sz);
                var it = JSON.parse(js);
                obj.series.push(it);
                var pnt = it.points;
                it.points = [];
                while (pnt > 0) {
                    offs += sz;
                    var date = new Int64(dv.getInt32(offs + 12, true), dv.getUint32(offs + 8, true)).toNumber();
                    var diff = dv.getInt32(offs + 16, true);
                    var cnt2 = Math.floor((dv.getUint32(offs, true) - 8) / 8);
                    var points_offs = offs + (cnt2 > 1 ? 20 : 16);
                    for (var j = 0; j < cnt2; j++) {
                        var val = dv.getFloat64(points_offs + j * 8, true);
                        val = [date, val];
                        if (includes_job) {
                            val.push(dv.getUint32(extra_offs, true));
                            extra_offs += 4;
                        }
                        if (includes_timestamp) {
                            val.push(new Int64(dv.getInt32(extra_offs + 4, true), dv.getUint32(extra_offs, true)).toNumber());
                            extra_offs += 8;
                        }
                        it.points.push(val);
                        date += diff;
                    }
                    pnt -= cnt2;
                    sz = 8 + dv.getUint32(offs, true);
                }
            }
            return obj; //JSON.stringify(obj);
        };

        this.sjts_request = function (url, url_args, method, post_data, success_callback, error_callback, options) {
            var url_params = this._prepareUrlArgs(url_args, true);

            var contentType = null;

            // -------------- BUILD REQUEST ---------------
            var xhr = new XMLHttpRequest();

            var baseUrl = self.options.api_server + '/api/1';
            if (options && options.baseUrl) {
                baseUrl = options.baseUrl;
            }

            var url_full = baseUrl + url + (url_params == null ? '' : "?" + url_params);
            xhr.open(method, url_full, self.async);

            var credentials = self.options.get_credentials();

            if (!options || options.ignoreAuth !== true) {
                if (credentials.api_username != '' && credentials.api_username != null) {
                    var auth = "Basic " + btoa(credentials.api_username + ":" + credentials.api_secret);
                    if ((options && options.authHeader) || credentials.sjauth) {
                        xhr.setRequestHeader("Sj-Auth", auth);
                    } else {
                        xhr.setRequestHeader("Authorization", auth);
                    }
                }
            }

            xhr.setRequestHeader('Sj-Receive-Format', 'sjts');
            xhr.responseType = "arraybuffer";

            if (options && options.contentType) {
                xhr.setRequestHeader("Content-Type", options.contentType);
            }

            if (typeof window !== 'undefined') {
                console.log('SJTS request start');
            }
            var counter = 0;
            xhr.onreadystatechange = function () { // (3)
                if (xhr.readyState != 4) return;
                var networkSpeed;
                if (!options || options.ignoreSpeed !== true) {
                    networkSpeed = self.getNetworkSpeed(xhr, startTime);
                }
                var response = null;
                if (xhr.response && xhr.response.byteLength) {
                    if (xhr.status == 200) {

                        var isJson = false;
                        try {
                            var format = xhr.getResponseHeader('Sj-Send-Format');
                            isJson = format === 'json';
                            if (typeof window !== 'undefined') {
                                console.log('format: ' + format + '');
                            }
                        } catch (ex) {
                            if (typeof window !== 'undefined') {
                                console.log('Unable to get format', ex);
                            }
                        }

                        if (isJson) {
                            if (typeof window !== 'undefined') {
                                console.log('Force to JSON from SJTS');
                            }
                            var responseText = new Uint8Array(xhr.response).reduce(function (data, byte) {
                                return data + String.fromCharCode(byte);
                            }, '');
                            try {
                                response = JSON.parse(responseText);
                            } catch (e) {
                                if (typeof window !== 'undefined') {
                                    console.log('unable to parse json response');
                                }
                            } finally {
                                if (!format && self.options && self.options.log_message) {
                                    if (typeof window !== 'undefined') {
                                        if (response) {
                                            console.log('Response: ' + JSON.stringify(response).substring(0, 100));
                                        } else {
                                            console.log('Response - blank');
                                        }
                                    }
                                    self.options.log_message('Missed Sj-Send-Format for SJTS request.');
                                }
                            }
                        } else {

                            try {
                                counter++;
                                response = self.sjts_parse(xhr.response, counter);
                            } catch (e) {
                                if (typeof window !== 'undefined') {
                                    console.log('SJTS request exception ' + (e || '[]').toString());
                                }
                                error_callback && error_callback(null, networkSpeed);
                                throw e;
                            } finally {
                                if (!format && self.options && self.options.log_message) {
                                    if (typeof window !== 'undefined') {
                                        if (response) {
                                            console.log('Response: ' + JSON.stringify(response).substring(0, 100));
                                        } else {
                                            console.log('Response - blank');
                                        }
                                    }
                                    self.options.log_message('Missed Sj-Send-Format for SJTS request.');
                                }
                            }
                        }
                    }
                }
                if (xhr.status == 200) {
                    if (typeof window !== 'undefined') {
                        console.log('SJTS request success');
                    }
                    if (success_callback) {
                        processResponse(url, url_args, method, post_data, response);
                        success_callback(response, networkSpeed);
                    }
                } else {
                    if (typeof window !== 'undefined') {
                        console.log('SJTS request error status ' + xhr.status);
                    }
                    error_callback && error_callback(response, networkSpeed);
                }
            }
            var startTime = new Date().getTime();
            post_data = post_data ? JSON.stringify(post_data) : null;
            xhr.send(post_data);
            return xhr;
        };

        this._scrollRequest = function (scroll_id, queryParams, xtra, series_callback, error_callback, finish_callback, requestOptions) {
            var self = this;
            this.request('/series', 'GET', {
                scroll_id: scroll_id
            }, null, function (response) {
                var stopProcessing = false;
                if (response.success && response.series && response.series.length) {
                    xtra.total += response.series.length;
                    stopProcessing = (typeof xtra.max_series == 'number' && xtra.total >= xtra.max_series);
                    response.series.forEach(function (s) {
                        series_callback(s);
                    });
                } else {
                    stopProcessing = true;
                }
                if (!stopProcessing) {
                    stopProcessing = response.series.length < queryParams.scroll_batch_size;
                }
                if (!stopProcessing) {
                    self._scrollRequest(scroll_id, queryParams, xtra, series_callback, error_callback, finish_callback, requestOptions);
                }
                if (response.success) {
                    if (stopProcessing) {
                        if (finish_callback) {
                            finish_callback();
                        }
                    }
                } else {
                    error_callback(response);
                }
            }, error_callback);
        };

        this.scroll = function (queryParams, series_callback, error_callback, finish_callback, requestOptions) {
            var self = this;

            queryParams = Object.assign({}, queryParams);

            if (queryParams.df && typeof queryParams.df == 'object' && queryParams.df.getTime) {
                queryParams.df = queryParams.df.getTime();
            }
            if (queryParams.dt && typeof queryParams.dt == 'object' && queryParams.dt.getTime) {
                queryParams.dt = queryParams.dt.getTime();
            }

            queryParams = Object.assign({
                scroll_batch_size: 1000
            }, JSON.parse(JSON.stringify(queryParams)));

            /*
            query: '',
            fields: '',
            df: '', //if data type is Date(), must convert to milli using utc conversion before passing
            dt: '', //if data type is Date(), must convert to milli using utc conversion before passing
            max_points: -1,
            sort: '',
            max_series: 300, //this is NOT passed to API -- once we hit this number of series results, we should stop scrolling; by default it is not limited (null means not limited)
            scroll_batch_size: 1000, //we should default to 1000 -- it's a good default setting and allows to finish scrolling reliably - users can change this though
            extra_params:
            */

            var paramNames = Object.keys(queryParams);
            var badParamKeys = [];
            var allowedParamKeys = {
                "query": 1,
                "fields": 1,
                "df": 1,
                "dt": 1,
                "max_points": 1,
                "sort": 1,
                "max_series": 1,
                "scroll_batch_size": 1,
                "extra_params": 1
            };
            paramNames.forEach(function (k) {
                if (!allowedParamKeys[k]) {
                    badParamKeys.push(k);
                }
            });

            if (badParamKeys.length) {
                error_callback({
                    success: false,
                    error: 'bad_params',
                    description: '"' + badParamKeys.join(',') + '" not allowed'
                });
                return;
            }

            queryParams = Object.assign(queryParams, {
                scroll: 'y'
            });

            var max_series = queryParams.max_series;
            delete queryParams.max_series;

            var xtra = {
                total: 0,
                max_series: max_series
            };

            this.request('/series', 'GET', queryParams, null, function (response) {
                if (response.success && response.scroll_id) {
                    var stopProcessing = false;
                    if (!xtra.max_series || xtra.max_series > response.total) {
                        xtra.max_series = response.total;
                    }
                    if (response.series && response.series.length) {
                        xtra.total += response.series.length;
                        response.series.forEach(function (s) {
                            series_callback(s);
                        });
                    } else {
                        stopProcessing = true;
                    }
                    if (!stopProcessing) {
                        stopProcessing = typeof xtra.max_series == 'number' && xtra.total >= xtra.max_series;
                    }
                    if (!stopProcessing) {
                        stopProcessing = response.series.length < queryParams.scroll_batch_size;
                    }

                    if (!stopProcessing) {
                        self._scrollRequest(response.scroll_id, queryParams, xtra, series_callback, error_callback, finish_callback, requestOptions);
                    }

                    if (stopProcessing) {
                        if (finish_callback) {
                            finish_callback();
                        }
                    }
                } else {
                    error_callback(response);
                }
            }, error_callback, requestOptions);
        };

        /**
         * options:
         *  endpoint - ws/wss endpoint
         *  message: function(data){ ... }, //called on non-error message
         *  error: function(message){  ... }, //called on error messages
         *  close: function(data: {code, reason, wasClean}){ ... } //called when connection is closed
         */
        this.ws = function (options) {
            var self = this;

            var debug = false;
            try {
                debug = localStorage.getItem('sjclient.ws.debug');
            } catch (e) { }

            var ready = false;
            var subscriptions = {};
            var pendingOperations = [];

            function onReady(success) {
                pendingOperations.forEach(function (operation) {
                    subscribe(operation.action, operation.callback);
                });
                pendingOperations.length = 0;
            }

            var socket = new WebSocket(self.options.api_server.replace(/^http/g, 'ws') + '/ws');
            socket.onopen = function () {
                if (debug) {
                    console.log("Connection established.");
                }
                var credentials = self.options.get_credentials();
                socket.send(JSON.stringify({ 'action': 'auth', 'user': credentials.api_username, 'api_key': credentials.api_secret }));
            };

            socket.onmessage = function (event) {
                if (debug) {
                    console.log("Data received " + event.data);
                }
                var data = event.data
                try {
                    data = JSON.parse(data);
                } catch (e) { }

                if (options && options.message) {
                    options.message(data);
                }
                if (!ready) {
                    ready = true;
                    onReady(data.success);
                } else {
                    // try to find subscription and call callbacks
                    var topicKey = getResponseTopicKey(data);
                    var topicSubs = subscriptions[topicKey];
                    if (topicSubs && topicSubs[data.query]) {
                        topicSubs[data.query].forEach(function (callback) {
                            callback(data);
                        });
                    }
                }
            };

            socket.onerror = function (error) {
                if (debug) {
                    console.log("Error: " + error.message);
                }
                if (options && options.error) {
                    options.error(data.message);
                }
                if (!ready) {
                    ready = true;
                    onReady(false);
                }
            };

            socket.onclose = function (event) {
                if (debug) {
                    if (event.wasClean) {
                        console.log('Connection closed');
                    } else {
                        console.log('Connection closed unexpectedly');
                    }
                    console.log('Error: ' + event.code + ' Description: ' + event.reason);
                }
                if (options && options.close) {
                    options.close({
                        code: event.code,
                        reason: event.reason,
                        wasClean: event.wasClean
                    });
                }
            };

            function getRequestTopicKey(action) {
                return ''; /* action.topic */  // TODO set topic key when it will be returned in response
            }
            function getResponseTopicKey(data) {
                return ''; /* data.type */  // TODO set topic key when it will be returned in response
            }

            /**
             * Subscribe action
             */
            function subscribe(action, callback) {
                if (callback) {
                    var topicKey = getRequestTopicKey(action);

                    var topicSubs = subscriptions[topicKey];
                    if (!topicSubs) {
                        topicSubs = {};
                        subscriptions[topicKey] = topicSubs;
                    }

                    var querySubs = topicSubs[action.query];
                    if (!querySubs) {
                        querySubs = [];
                        topicSubs[action.query] = querySubs;
                    }

                    querySubs.push(callback);
                }

                socket.send(JSON.stringify(action));
            }

            /**
             * Unsubscribe action
             */
            function unsubscribe(action, callback) {
                // TODO: unsubscribe in ws

                // unsubscribe callback
                if (callback) {
                    var topicKey = getRequestTopicKey(action);
                    var topicSubs = subscriptions[topicKey];
                    if (topicSubs && topicSubs[action.query]) {
                        var subs = topicSubs[action.query];
                        var index = subs.indexOf(callback);
                        if (index != -1) {
                            subs.splice(index, 1);
                        }
                    }
                }
            }

            return {
                /**
                 * Subscribe to topic query change. Returns unsubscribe method
                 */
                subscribe: function (topic, query, callback) {
                    var action = { 'action': 'subscribe', 'topic': topic, 'query': query };
                    if (ready) {
                        subscribe(action, callback);
                    } else {
                        pendingOperations.push({
                            action: action,
                            callback: callback
                        });
                    }
                    return function () {
                        unsubscribe(action, callback);
                    };
                },
                /**
                 * Close connection
                 */
                close: function () {
                    pendingOperations.length = 0;
                    subscriptions = {}; // clear all subscriptions
                    socket.close();
                }
            }
        }

        // Scope for raw request methods
        this.raw = {
            // Pseudonym for GET request
            get: function (method_url, args, success, error) {
                return self.request(
                    method_url,
                    'GET',
                    args,
                    null,
                    success,
                    error
                );
            },

            // Pseudonym for POST request
            post: function (method_url, url_args, post_data, success, error) {
                return self.request(
                    method_url,
                    'POST',
                    url_args,
                    post_data,
                    success,
                    error
                );
            }
        };

        this._setLocationType = function (locationType) {
            this._locationType = locationType;
        };

        this.setLocation = function (location) {
            this._location = location;
        };
    };

    var constants = {
        escapeCharsExp: /[~><\*(),:=\@;&\s]/
    };

    ShoojuClient.helpers = helpers;
    ShoojuClient.constants = constants;

    return ShoojuClient;
})));