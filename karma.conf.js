module.exports = function (config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
      './node_modules/sinon/pkg/sinon.js',
      './node_modules/jasmine-sinon/lib/jasmine-sinon.js',
      'shooju.js',
      'tests/*.test.js'
    ],
    reporters: ['progress'],
    port: 9876,
    colors: true,
    browsers: ['Chrome'],
    logLevel: config.LOG_INFO,
    autoWatch: true,
    concurrency: Infinity
  })
}