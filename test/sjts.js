
global.XMLHttpRequest = require("xhr2").XMLHttpRequest;
global.btoa = function(str) {
    return Buffer.from(str).toString('base64');
}

var shoojuClient = require("shooju-client");
var server = process.env['ShoojuTests.Server'];
var user = process.env['ShoojuTests.User'];
var api_key = process.env['ShoojuTests.ApiSecret'];

var sjclient = new shoojuClient(server, user, api_key);

var success_cb = function (response) {
    console.log("success: " + response);
    console.log("series: " + response.series.length);
    console.log("points: " + response.series[0].points.length);
    console.log(JSON.stringify(response));
};

var error_cb = function (response) {
    console.log("error: " + response);
};

sjclient.jsonpinmsie = false;
sjclient.sjts_request('/series', {'query': '', 'max_points': 10}, 'GET', null, success_cb, error_cb);

